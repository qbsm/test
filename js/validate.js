function _scrollToMe(me, num) {
    var _top = parseInt(me.offset().top) + num + 'px';
    $('html,body').animate({scrollTop: _top}, 'slow');
}

function _scrollToTarget(target) {
    var _top = parseInt(target.offset().top) + 'px';
    $('html,body').animate({scrollTop: _top}, 'slow');
}

/*
 Валидация формы
 */
$(function () {

    $('form.orderform').each(function () {
        $(this).validate({
            focusInvalid: false,
            focusCleanup: true,
            ignore: "",
            rules: {
                name: {required: true},
                phone: {required: true},
                salon: {
                    required: true,
                    range: [1, 10]
                }
            },
            messages: {
                name: {required: "Вы забыли представиться"},
                phone: {required: "Введите номер телефона"},
                salon: {
                    required: "Выберите автосалон",
                    range: "Выберите автосалон"
                }
            },
            errorPlacement: function (error, element) {
                var er = element.attr("name");
                error.appendTo(element.parent().find("span.red_text"));
            }
        });
    });

    $.mask.definitions['d'] = '[1-69]';
    $('input, textarea').placeholder();
    $('input.phone').mask("+7 (d99) 999-99-99", {placeholder: "_"});
});


/*
 Общие для всей страницы скрипты
 */
$(function () {

    $('#oferta-button').click(function () {
        if ($('#oferta-content').is(':hidden')) {
            var $me = $(this);
            $('#background-fader').css({height: $(document).height()}).fadeIn('slow', function(){
                $('#oferta-content').slideDown('slow');
                $('#close-oferta').unbind('click').bind('click', function(){
                    $('#oferta-button').click();
                    return false;
                });
                $('#close-oferta').show();
                _scrollToTarget($me);
            });

        } else {
            $('#oferta-content').slideUp('slow', function(){
                $('#background-fader').fadeOut('slow');
                $('#close-oferta').hide();
            });
        }
        return false;
    });

    $('.model-summary').click(function () {
        $('.model-summary').removeClass('active');
        $(this).addClass('active');

        $('#mod-form').hide();

        var title = $(this).attr('data-title');
        var model = $(this).attr('data-model');

        $('#mttl').html(title);
        $('input[name="model"]').val(model);

        $('#mod-form').show();

        // _scrollToTarget($(this));

        return false;
    });

    $('#sender').click(function(){
        var $form = $(this).parents('form').eq(0);
        $form.validate();

        if ($form.valid())
        {
            var model = $('input[name="model"]').val();
            var brand = $('input[name="brand"]').val();
            $.ajax({
                type: 'POST',
                data: $form.serialize(),
                url: '/ajax/forms-80-new.php',
                dataType: 'json',
                success: function(data){
                    if (data.status==true) {
                        _gaq.push(['_trackPageview', '/vernem80_'+brand+'_'+model+'_ok.html']);
                        $form.slideUp().parent().append('<h4>'+data.message+'</h4>');
                    } else {
                        $.ajax({
                            type: 'POST',
                            data: $form.serialize(),
                            url: '/ajax/forms-80-new.php?report=1'
                        });
                    }
                }
            });
        }
        else
        {
            $.ajax({
                type: 'POST',
                data: $form.serialize(),
                url: '/ajax/forms-80-new.php?report=1'
            });
        }
        return false;
    });

    $('.model-summary').eq(0).click();

});